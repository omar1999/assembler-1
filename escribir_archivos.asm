; Omar Sanmartin
; Fecha 19 de Agosto de 2020
%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

segment .data
    msj1 db "Ingrese datos en el archivo",10
    len_msj1 equ $-msj1

    archivo db "/home/omar1999/Documentos/emsamblador/assembler-1/archivo.txt"

segment .bss
    texto resb 100
    len_texto equ $-texto 
    idarchivo resd 1

segment .text
    global _start

leer:
    mov eax,3
    mov ebx,0
    mov ecx,texto
    mov edx,len_texto
    int 80h
    ret

_start:

    mov eax,8           ;servicio para crear archivos
    mov ebx,archivo     ;direccion de archivo
    ;mov     ecx, 0o777          ; set all permissions to read, write, execute
    mov ecx,1          ;Modo de Acceso
                            ;O-RDONLY 0:El archivo de abre solo para leer
                            ;O-WRONLY 1:El archivo de abre para escritura
                            ;O-RDWR   2:El archivo se abre para escritura y lectura
                            ;O-CREATE 256:Crear el archivo en caso que no exista
                            ;O-APPEND 2000h:El archivo se abre solo para escritura final
    mov edx, 777h
    int 80h

    test eax,eax        ;Instruccion de comparacion realiza la operacion logica "Y" de dos operandos
                        ;pero No afect a ninfguno de ellos,Solo afecta al registro de estado.
                        ;Admite todos los tipos de direccionamiento excepto los dos operandos de memoria
                            ; TEST reg,reg
                            ; TEST reg mem
                            ; TEST mem,reg
                            ; TEST reg, inmediato
                            ; TEST mem, inmediato

    jz salir                ;Se ejecuta cuando existen errores en el archivo
    
    mov dword[idarchivo],eax
    imprimir msj1,len_msj1


    call leer
    ;*************** Escritura en el archivo***************
    mov eax,4
    mov ebx,[idarchivo]
    mov ecx,texto
    mov edx,len_texto
    int 80h

salir:
    mov eax,1
    int 80h