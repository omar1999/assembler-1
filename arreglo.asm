segment .data
        arreglo db 3,2,0,7,5
        len_arreglo equ $-arreglo
        new_line db 10,''
segment .bss
        numero resb 1
segment .text
    global _start

_start:
    mov esi,arreglo     ; esi = fijar tamaño del arreglo, posicionar el arreglo de memeoria 
    mov edi,0           ; edi = contener el indice del arreglo

imprimir:
    mov al,[esi]
    add al,'0'
    mov [numero],al

    add esi,1
    add edi,1   
    
    mov eax,4
    mov ebx,1
    mov ecx,numero
    mov edx,1
    int 80h     

    mov eax,4
    mov ebx,1
    mov ecx,new_line
    mov edx,1
    int 80h    

    cmp edi,len_arreglo ;Cmp 3,4 => Activa Carry
                        ;Cmp 4,3 => Desactiva Carry y zero
                        ;Cmp 4,4 => Desactiva Carry y zero se activa
    jb imprimir         ;Se ejecuta la bandera de carry esta activada
    

salir:
    mov eax,1
    int 80h