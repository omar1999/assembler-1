section .data
	mensaje db "Mi primera vez con NASM"  
	longitud EQU $-mensaje		     
section .bss

section .text
	global_start
_start:
	;**************imprimir el mensaje**********************
	mov eax, 4	        ;Funcion sys_write
	mov ebx, 1          ;Tipo de salida | Pantalla
	mov ecx, mensaje	;Referencia a la memoria de la variable mensaje
	mov edx, longitud 	;Referencia a la memoria de la longitud de la variable mensaje
	int 80H			    ;Interrupcion de software para Linux para imprimir 

	mov eax, 1		    ;Salida del programa, funcion sys_exit
	int 80H             ;Interrupcion para cerrar el programa
