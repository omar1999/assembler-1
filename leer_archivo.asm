; Omar Sanmartin
; Fecha 5 de Agosto de 2020
%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

segment .data
    msj1 db "Leer el archivo",10
    len_msj1 equ $-msj1

    archivo db "/home/omar1999/Documentos/emsamblador/assembler-1/archivo.txt"

segment .bss
    texto resb 100
    len_texto equ $-texto 
    idarchivo resd 1

segment .text
    global _start
_start:
    ;**************Abrir el archivo********
    mov eax,5 
    mov ebx,archivo 
    mov ecx,0        
    mov edx, 777h
    int 80h

    test eax,eax           
    jz salir 
    ;*********Testear***********
    mov dword[idarchivo],eax
    imprimir msj1,len_msj1

    ;**********leer el archivo***********
    mov eax,3  
    mov ebx,[idarchivo]
    mov ecx,texto       
    mov edx,len_texto
    int 80h

    imprimir texto,len_texto

    ; ************************** cerrar el archivo **************************
	mov eax, 6		; close
	mov ebx, [idarchivo]	
	mov ecx, 0		
	mov edx, 0
	int 80h

salir:
    mov eax,1
    int 80h