section .data
 # Omar Alexis Sanmartin   
	resul db 'El resultado de la resta 9 - 5 es',10
	lonresul equ $-resul
    line db '',10
    len_line equ $-line

section .bss
	numero resb 1

section .text
	global _start

_start:
        mov eax,9
        mov ebx,5
        sub eax,ebx  ; eax = eax + ebx
        add eax, '0'    ;Transformamos a entero o numero enves que se quede en ascii

        mov [numero],eax

        mov eax,4
        mov ebx,1
        mov ecx,resul
        mov edx,lonresul
        int 80H

        mov eax,4
        mov ebx,1
        mov ecx ,numero
        mov edx,1
        int 80H

        mov eax,4
        mov ebx,1
        mov ecx ,line
        mov edx,len_line
        int 80H
        

        mov eax,1
        int 80H