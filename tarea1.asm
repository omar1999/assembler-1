;Comentar el significado de cada línea
				;Funcion que imprime en consola
%macro imprimir 2 ;define instrucciones e indica que va a recibir dos parametros 
	mov eax,4	;Transfiere 4 a eax; Funcion para escribir
	mov ebx,1	;Transfiere 1 a ebx; Salida estandar por pantalla
	mov ecx,%1 	;Valor del primer parametro ; indica la direccion de la variable a imprimir
	mov edx,%2	;Valor del segundo parametro; indica la longitud de la variable a imprimir
	int 80H		;Interrupcion para linux
%endmacro
				;Funcion que lee datos de consola
%macro leer 2	;el macro define instrucciones va a recibir dos parametros 
    mov eax,3	;Transfiere 3 a eax;Funcion para leer 
    mov ebx,0	;Transfiere 0 a ebx;Entrada estandar por teclado
    mov ecx,%1 	;Valor del primer parametro ; indica el segmento donde se guardara el valor a leer
    mov edx,%2	;Valor del segundo parametro; indica el sgemento para guardar el tamaño de el valor
    int 80H 	;Interrupcion para linux
%endmacro


; ecx,modo de acceso
; edx, permisos
section .bss   ; Los datos no iniciados se colocan en el segmento .bss
	auxiliar resb 30	;Se crean variables para reservar memoria
	auxiliarb resb 30	; ya que no estan incializadas de tamaño 30 
	auxiliarc resb 30


section .data	; Los datos iniciados se colocan en el segmento .data
	msg db 0x1b ,"       " ; 6 espacios para contener al dato
	lenmsg equ $-msg		; almacena la longitud de la variable msg


	salto db " ",10 		;se crea un salto de linea
	lensalto equ $-salto	;se define a longitud de la variable salto


section .text				;El código se coloca en el segmento .text
    global _start    

_start:						;Declara el inicio
	 
	mov ecx,9			;Transfiere el valor 9 al segmento ecx

	mov al,0			;Para transferir un valor a auxiliar utilizamos el segmento al como una variable auxiliar
	mov [auxiliar],al   ;Transferimos del segemento a la variable llamada auxiliar

cicloI:					;Etiqueta que agrupa instrucciones a ejecutarse
	push ecx			;Introduce ecx a la pila
	mov ecx,9			;Transfiere el valor 9 al segemento ecx

	mov al,0			;Para transferir un valor a auxiliar utilizamos el segmento al como una variable auxiliar
	mov [auxiliarb],al	;Transferimos del segemento a la variable llamada auxiliar

	cicloJ:				;Etiqueta que agrupa instrucciones a ejecutarse
		push ecx			;Introduce ecx a la pila
		call imprimir0al9	;Llama a una subrutina llamada imprimir0al9
							;Esta subrutina se ejecutara e imprimira valores
		
	;	imprimir msg2,lenmsg2

	fincicloJ:				;Etiqueta que agrupa instrucciones a ejecutarse
		mov al,[auxiliarb]	;Tranferencia de la variable auxiliarb al segmento al
		inc al				;Incrementa el registro al en 1
		mov [auxiliarb],al	;Se tranfiere el valor del segmento al anteriormente incrementado a la variable auxiliarb

		pop ecx				;Extraer el ultimo valor que se le puso por ultima vez
		loop cicloJ			;Realiza un bucle con cicloJ y se detiene hasta que cx sea 0;
							;Al hacer un bucle loop el registro cx se decrementa implicitamente hasta llegar a 0
							;El loop se ejecuta 9 veces ya que cx fue definido como 9 anteriormente
		
	;imprimir salto,lensalto

fincicloI:					;Etiqueta que agrupa instrucciones a ejecutarse
	mov al,[auxiliar]		;Tranferencia de la variable auxiliar al segmento al
	inc al					;Incrementa el registro al en 1
	mov [auxiliar],al		;Se tranfiere el valor del segmento al anteriormente incrementado a la variable auxiliar

	pop ecx					;Extraer el ultimo valor que se le puso por ultima vez
	loop cicloI				;Realiza un bucle con cicloI y se detiene hasta que cx sea 0;
							;Al hacer un bucle loop el registro cx se decrementa implicitamente hasta llegar a 0
							;El loop se ejecuta 9 veces ya que cx fue definido como 9 anteriormente
	

salir:						;Etiqueta que agrupa instrucciones a ejecutarse para salir
	mov eax, 1				;Funcion para salir del sistema
	int 80H					;Interrupcion para linux



imprimir0al9:				;Etiqueta que agrupa instrucciones a ejecutarse 
							;Estas instrucciones imprimen en pantalla ya que al final llaman al macro 
							;imprimir y le envian parametros
							;Por los loops esta instruccion se repetira 81   
	
	mov ebx,"["				;Transfiere el caracter al segmento ebx
	mov [msg+1], ebx		;Transfiere el valor del segmento a la constante en la posicion 1

	mov bl,[auxiliar]		;Trasfiere la variable auxiliar al segemento bl
	add bl,'0'				;Se transforma en cadena 
	mov [msg+2], bl			;Tranfiere el valor del segmento a la constante en la posicion 2


	mov ebx,";"				;Transfiere el caracter al segmento ebx
	mov [msg+3], ebx		;Tranfiere el valor del segmento a la constante en la posicion 3

	
	mov bl,[auxiliarb]		;Trasfiere la variable auxiliarb al segemento bl
	add bl,'0'				;Se transforma en cadena 
	mov [msg+4],bl			;Tranfiere el valor del segmento a la constante en la posicion 4

	mov ebx,"fJ"			;Transfiere el caracter al segmento ebx
	mov [msg+5], ebx		;Tranfiere el valor del segmento a la constante en la posicion 5

	imprimir msg,lenmsg		;Se envia los parametros al macro para que ejecute las instrucciones con estos valores

	ret						;Instruccion para el retorno
