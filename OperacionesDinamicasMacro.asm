;Omar Alexis Sanmartin Tapia
;Operaciones Dinamicas Macro
;Sexto Ciclo Paralelo A
%macro escritura 2
    mov eax, 4
    mov ebx, 1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

%macro lectura 2
    mov eax,3
    mov ebx,2
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

section .data
	numero db 'Ingrese el primer numero',10
	lennumero equ $-numero

    numerodos db 'Ingrese el segundo numero',10
	lennumerodos equ $-numerodos

    respuesta db 'La respuesta es',10
	lenrespuesta equ $-respuesta

    suma db 'Suma',10
	lensuma equ $-suma

    resta db 10,'Resta',10
	lenresta equ $-resta

    division db 10,'Division',10
	lendivision equ $-division

    residuo db 10,'Residuo',10
	lenresiduo equ $-residuo

    multiplicacion db 10,'Multiplicacion',10
	lenmultiplicacion equ $-multiplicacion

    salto db ' ',10
	lensalto equ $-salto


section .bss
    numeroouno resb 1
    numeroodos resb 1
	sumaa resb 1
    restaa resb 1
    divisionn resb 1
    residuoo resb 1
    multiplicacionn resb 1

section .text
	global _start
_start:
    escritura numero,lennumero
    lectura numeroouno,2
    escritura numerodos,lennumerodos
    lectura numeroodos,2


    mov al, [numeroouno]
    mov bl, [numeroodos]
    sub al, '0'
    sub bl, '0'
    mov [numeroouno],al
    mov [numeroodos],bl


    mov al, [numeroouno]
    mov bl, [numeroodos]
    add al, bl
    add al, '0'
    mov [sumaa],al


    mov al, [numeroouno]
    mov bl, [numeroodos]
    sub al, bl
    add al, '0'
    mov [restaa],al

    mov al, [numeroouno]
    mov bl, [numeroodos]
    mul bl
    add al, '0'
    mov [multiplicacionn],al

    mov al, [numeroouno]
    mov bl, [numeroodos]
    div bl
    add al, '0'
    add ah, '0'
    mov [divisionn],al
    mov [residuoo],ah

    escritura suma,lensuma
    escritura sumaa,1

    escritura resta,lenresta
    escritura restaa,1

    escritura multiplicacion,lenmultiplicacion
    escritura multiplicacionn,1

    
    escritura division,lendivision

    escritura divisionn,1
    escritura residuo,lenresiduo
    
    escritura residuoo,1
    escritura salto,lensalto
    
    
    mov eax,1
	int 80h