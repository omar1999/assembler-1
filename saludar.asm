section .data
	mensaje db "Mi primera vez con NASM"  ;constante mensaje un byte en memoria
	longitud EQU $-mensaje		      ;constante longitud que calcula el # caracteres de mensaje
;section .bss

section .text
	global_start
_start:
	;**************imprimir el mensaje**********************
	mov eax, 4	;por lo pronto es un movimiento de un dato al registro eax
	mov ebx, 1      ;por lo pronto es un movimiento de un dato al registro eax
	mov ecx, mensaje	;por lo pronto es un movimiento de un dato al registro ecx
	mov edx, longitud 	;por lo pronto es un movimiento de un dato al registro edx
	int 80H			;interrupcion de software para el sistema operativo linux

	mov eax, 1		;salida del programa, system_exit, sys_exit
	mov ebx, 0		;si el retorno es 0 (200 en la web ) ok
	int 80H
