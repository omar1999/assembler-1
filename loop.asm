%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
    saludo db 'Hola '
    len equ $-saludo

    salto db 10
    lensalto equ $-salto

section .bss
    iterador resb 1

section .txt
    global _start
_start:
    mov ecx,9

for:
    push ecx

    imprimir saludo,len

    pop ecx
    push ecx
    add ecx,'0'
    mov [iterador],ecx

    imprimir iterador,1

    
    imprimir salto,lensalto

    pop ecx
    loop for

salir:
    mov eax,1
    int 80h