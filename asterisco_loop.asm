; Omar Sanmartin
%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
    asterisco db '*'
    nueva_linea db 10,''
section .text
    global _start
_start:
    mov ecx,8
    mov ebx,1
l1:
    push ecx
    push ebx
    ;************ nueva linea *************
    mov ecx,ebx
l2:
    push ecx
    ;************ asterisco ***************
    imprimir asterisco,1
    pop ecx
    loop l2
    ;************ finaliza loop columnas *******
    imprimir nueva_linea,1
    pop ebx
    pop ecx
    inc ebx
    loop l1
    ;*********** finalizo las filas ************
    mov eax,1
    int 80h
