; Omar Sanmartin
%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
    msj1 db ' * '
    len_msj1 equ $-msj1
    
    msj2 db ' = '
    len_msj2 equ $-msj2
    
    linea db 10
    len equ $-linea

    ;salto db 10
section .bss
	a resb 1
    b resb 1
    c resb 2
section .txt
    global _start
_start:

    mov ax,3
    add ax,'0'
    mov [a],ax
    mov cx,1

proceso:
    push cx
    push cx

    add cx,'0'
    mov [b],cx 
        
    mov ax,3
    
    pop cx 
    mul cx
    aam
    add al,'0'
    add ah,'0'
    mov [c+0],ah
    mov [c+1],al
    
    
    call print
    pop cx
    inc cx
    cmp cx,9
    jnz proceso
    jmp salir

print: 
    imprimir a,1
    imprimir msj1,len_msj1
    imprimir b,1
    imprimir msj2,len_msj2
    imprimir c,2
    imprimir linea,len
    ret

salir:
    mov eax,1
    int 80h