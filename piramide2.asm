; Omar Sanmartin
%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
    saludo db '*'

    salto db 10
    lensalto equ $-salto

section .txt
    global _start
_start:
    mov ecx,9
l0:
    push ecx
    ;mov ecx,9
l1: 
    push ecx
    imprimir saludo,1
    pop ecx
    loop l1
l2:
    imprimir salto,lensalto
    pop ecx
    loop l0
salir:
    mov eax,1
    int 80h