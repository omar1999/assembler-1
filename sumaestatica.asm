;Suma estatica
section .data
    resultado db 'El resultado es :',10
    len equ $-resultado
    line db '',10
    len_line equ $-line

section .bss
    suma resb 1
section .text
    global _start

_start:
        mov eax,6
        mov ebx,2
        add eax,ebx  ; eax = eax + ebx
        add eax, '0'    ;Transformamos a entero o numero enves que se quede en ascii

        mov [suma],eax

        mov eax,4
        mov ebx,1
        mov ecx,resultado
        mov edx,len
        int 80H

        mov eax,4
        mov ebx,1
        mov ecx ,suma
        mov edx,1
        int 80H

        mov eax,4
        mov ebx,1
        mov ecx ,line
        mov edx,len_line
        int 80H

        mov eax,1
        int 80H