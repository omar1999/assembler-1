section .data
	mensaje db 'Ingrese un numero',10
	tamano equ $-mensaje
	desplegar db 'El número ingresado es',10
	longitud equ $-desplegar

section .bss
	num resb 5

section .text
	global _start

_start:
	mov eax, 4
	mov ebx, 1
	mov ecx, mensaje
	mov edx, tamano
	int 80h

    

	mov eax, 3
	mov ebx, 2
	mov ecx, num
	mov edx, 5
	int 80h

    mov ebx, 7           ;7 hace refrencia a timbre en ascii
    add ebx, '0'         ;0=>48 ; se le suma a ebx;7+48=55; 55= 7
    mov [num] , ebx      ;Asignacion de un numero en variable

	mov eax, 4
	mov ebx, 1
	mov ecx, desplegar
	mov edx, longitud
	int 80h

	mov eax, 4
	mov ebx, 1
	mov ecx, num
	mov edx, 5
	int 80h

	mov eax,1
	int 80h
