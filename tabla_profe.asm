; Omar Sanmartin
; Fecha 5 de Agosto de 2020
%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
    msj1 db ' * '
    len_msj1 equ $-msj1
    
    msj2 db ' X '
    len_msj2 equ $-msj2

    msj3 db ' = '
    len_msj3 equ $-msj3

    msj4 db 10
    len_msj4 equ $-msj4
    
    espacio db 10
    len_espacio equ $-espacio
section .bss
    numero rest 2
    presentarN1 rest 2
    presentarN2 rest 2
    repuesta rest 2
    aux rest 2
    x rest 2
section .text
    global _start
_start:
    mov al,1
    mov [aux],al
    mov ecx,0

principal:
    cmp ecx,9
    jz bucle
    inc ecx
    push ecx
    mov [numero],ecx
    jmp imp
imp:
    mov al,[numero]
    mov [presentarN1],al
    mov cl,[aux]
    mul cl

    mov [numero],cl
    mov ah,[presentarN1]
    add ax,'00'
    add cx,'00'
    mov [presentarN1],ah
    mov [presentarN2],cl

    imprimir presentarN1,2
    imprimir msj2,len_msj2
    imprimir presentarN2,2
    imprimir msj3,len_msj3
    mov eax,48
    add [numero],eax
    imprimir numero,2
    imprimir espacio,1
    pop ecx
    jmp principal

bucle:
    imprimir espacio,len_espacio
    mov ebx,[aux]
    inc ebx
    mov [aux],ebx
    mov ecx,0
    cmp ebx,10
    jb principal; jnAE

salir:
    mov eax,1
    int 80h


