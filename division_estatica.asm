section .data
	mensaje db 'Ingrese un numero a dividir',10
	tamano equ $-mensaje

    mensajedos db 'Ingrese un numero 2 numero a dividir',10
	tamanodos equ $-mensajedos
    
	resultado db 'El resultado de la division es',10
	longitud equ $-resultado

section .bss
	num resb 1
    numdos resb 1
    respuesta resb 2

section .text
	global _start

_start:
    mov eax, 4
	mov ebx, 1
	mov ecx, mensaje
	mov edx, tamano
	int 80h

    mov eax, 3
	mov ebx, 2
	mov ecx, num
	mov edx, 2
	int 80h


    mov eax, 4
	mov ebx, 1
	mov ecx, mensajedos
	mov edx, tamanodos
	int 80h

    mov eax, 3
	mov ebx, 2
	mov ecx, numdos
	mov edx, 2
	int 80h

    mov al,[num]
    mov bl,[numdos]

    sub al,'0'        
    sub bl,'0'   
    div bl;  eax = eax / ebx
    add al,'0'
    mov [respuesta],al

    mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, longitud
	int 80h

    

	mov eax, 4
	mov ebx, 1
	mov ecx, respuesta
	mov edx, 2
	int 80h
 
	mov eax, 1
	int 80h
    
