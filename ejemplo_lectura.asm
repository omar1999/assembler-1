section .data
	mensaje db 'Ingrese un numero',10
	tamano equ $-mensaje
section .bss
	num resb 5

section .text
	global _start

_start:
	mov eax, 4          
	mov ebx, 1
	mov ecx, mensaje
	mov edx, tamano
	int 80h
    
	mov eax, 3      ;Funcion sys_read
	mov ebx, 2      ;Standar | Lectura por teclado
	mov ecx, num    ;Apunta a un área de memoria donde se dejarán los caracteres obtenidos
	mov edx, 5      ;Número máximo de caracteres a mostrar
	int 80h         ;Interrupcion de software para Linux para lectura 

	mov eax,1       ;Salida del programa,Funcion sys_exit
	int 80h         ;Interrupcion para cerrar el programa
